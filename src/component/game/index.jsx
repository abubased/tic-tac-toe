import React, { Component } from 'react'
import Board from './board'
import './index.css'
class Game extends Component {

  constructor(props){
    super(props)
    this.state = {
      history:[ {squares:Array(9).fill(null)} ],
      stepNumber:0,
      xTurn:true
    }
  }

  handleClick = (index)=>{
    let {history,stepNumber} = this.state;
    history = history.slice(0,stepNumber+1);
    let currentItem = history[history.length-1].squares.slice();
    let currentSquares = this.state.history[stepNumber].squares;
    let winner = this.calculateWinner(currentSquares);
    if(currentItem[index]===null && !winner){
      currentItem[index]=(this.state.xTurn)?"X":"O"
      console.log(currentItem[index]);
      this.setState({
        history:[...history,{squares:currentItem}],
        xTurn:!this.state.xTurn,
        stepNumber:history.length
      })
    }
  }
  
  calculateWinner = (squares)=>{
    const lines = [
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,4,8],
      [2,4,6],
      [0,3,6],
      [1,4,7],
      [2,5,8],
    ];
    let winner;
    lines.forEach(currentItem => {
      const [a,b,c] = currentItem
      if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){
        winner = squares[a];
      }
    });
    return winner;
  }

  goToStep = (index) => {
    this.setState({
      history:this.state.history,
      stepNumber:index,
      xTurn:index%2===0?true:false
    })
  }
  render() {
   let {history,stepNumber,xTurn} = this.state;
   let currentSquares = this.state.history[stepNumber].squares;
   let winner = this.calculateWinner(currentSquares);
    return (
      <div className="game">
        <div className="game-board">
          <Board history={history} xTurn={xTurn} handleClick={this.handleClick} winner={winner} stepNumber={stepNumber}/>
        </div>
        <div className="game-info">
          <ol>
            <GameInfo history={history} goToStep={this.goToStep}/>
          </ol>
        </div>
      </div>
    )
  }
}
const GameInfo = (props)=>{
  let {history,goToStep} = props;
  let listItem = history.map((element,index) =>{
    return(<li key={index.toString()}><button onClick={()=>{goToStep(index)}}> Go to {index===0 ? `Start` : `move #${index}`}</button></li>)
  });
  return listItem;
}
export default Game

