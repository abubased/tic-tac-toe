import React from 'react'
import Square from './square'
import './index.css'

export default function Board(props) {
  let {history,handleClick,winner,xTurn,stepNumber} = props;
  const renderSquare = (i)=>{
    let squares = history[stepNumber].squares;
    return(
      <Square 
        value={squares[i]}
        handleClick={handleClick}
        index={i}
      />
    )
  }
  let status;
  if (winner) {
    status = 'Winner is: ' + winner;
  } else {
    status = 'Next player is: ' + (xTurn ? 'X' : 'O');
  }
  return(
    <div className="borad">
      <div className="status">{status}</div>
      <div className="borad-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="borad-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="borad-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  )   
}


