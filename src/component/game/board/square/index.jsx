import React from 'react'
import './index.css'

const Square = (props) => {
  return (
    <button className="square" onClick={()=>{props.handleClick(props.index)}}>
    {props.value}
  </button>
  )
}
export default Square;
